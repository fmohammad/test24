package dev.freightos.test24.controller.flow.testcase;

import static org.junit.Assert.*;

import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

import dev.freightos.test24.controller.CustomerInterface;
import dev.freightos.test24.controller.HandleInstances;
import dev.freightos.test24.controller.SnacksMachineInterface;
import dev.freightos.test24.controller.flow.AppConf;
import dev.freightos.test24.model.Keypad;

public class InvalidCurrencyTest {
	
	
	
	public InvalidCurrencyTest() {
		BasicConfigurator.configure();

	}

	public  void populate(SnacksMachineInterface	snacksMachineInstance) {

		AppConf.populateForTest(snacksMachineInstance);
			
		}

		@Test		
		public  void test() {
			
			CustomerInterface customer=HandleInstances.getCustomerInterfaceInstance();
			
			
			SnacksMachineInterface	snacksMachineInstance =HandleInstances.getSnacksMachineInstance(customer);
			populate(snacksMachineInstance);

			Keypad keypad=snacksMachineInstance.getKeypad();
			keypad.setKeyJustpressed(1);
			
			snacksMachineInstance.chooseItemForPurchase(keypad);
			
			boolean isenough=false;
			
			while(!isenough) {
				try {
			  isenough=snacksMachineInstance.insertMoney(snacksMachineInstance.getCardSlot(), 100, "EUR");
				}catch(Exception e) {
					fail(e.getMessage());
					
				}
			}
			
			
			
			
		
		
	}


}
