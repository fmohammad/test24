package dev.freightos.test24.controller.flow.testcase;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;

import dev.freightos.test24.controller.CustomerInterface;
import dev.freightos.test24.controller.HandleInstances;
import dev.freightos.test24.controller.MoneySlot;
import dev.freightos.test24.controller.SnacksMachineInterface;
import dev.freightos.test24.controller.flow.AppConf;
import dev.freightos.test24.controller.flow.InvalidAmountOrCurrencyException;
import dev.freightos.test24.controller.flow.ItemNotAvialableException;
import dev.freightos.test24.model.Item;
import dev.freightos.test24.model.Keypad;


public class InvalidSlotAmountCombinationTest {



		public  void populate(SnacksMachineInterface	snacksMachineInstance) {

		AppConf.populateForTest(snacksMachineInstance);
			
		}
		
		
		/**
		 * Many Items selection Flow ~ Valid Scenario
	1. This use case begins when the customer wants to purchase snacks.
	2. The customer selects a number by pressing on the keypad. 
	3. The VM displays a message that the snack is available for the selected number and displays its price.
	4. The customer selects another number by pressing on the keypad. 
	5. 3,4 repeat many more than once until the customer satisfied with items to purchase
	6. The customer inserts the money.
	7. The VM validates the money.
	8. The VM accepts the money. 
	9. The VM displays the accumulated amount of money each time a new money is entered.
	10. The VM monitors the amount of the accepted money, If the money is enough, the VM dispenses the selected snack to the customer. 
	11. The VM determines if any change should be sent back to customer.
	12. The VM displays the change at panel. 
	13. Then, the VM dispenses change.	
		 */
		@Test		
		public  void handleManyItemsFlow() {
			
			CustomerInterface customer=HandleInstances.getCustomerInterfaceInstance();

			SnacksMachineInterface	snacksMachineInstance =HandleInstances.getSnacksMachineInstance(customer);
			populate(snacksMachineInstance);
			Keypad keypad=snacksMachineInstance.getKeypad();

			int select =0;
			boolean atLeastOneIsValid=false;
			/**
			 * keep in the loop until at least one available snack is selected 
			 */
			select=1;    

				try {
				keypad.setKeyJustpressed(select);
				snacksMachineInstance.chooseItemForPurchase(keypad);
				atLeastOneIsValid=true;
				}catch(ItemNotAvialableException e) {
					System.err.println("item not available");
					fail("item not available");
				}
			
			
			
			boolean isenough=false;
			MoneySlot coinSlot=snacksMachineInstance.getCoinSlot();
			MoneySlot notesSlot=snacksMachineInstance.getNotesSlot();
			MoneySlot cardSlot=snacksMachineInstance.getCardSlot();
			MoneySlot usedSlot=null;
			
			
				System.out.println("Enter amount to use");
				
				float amt=1.2f;
				
				System.out.println("Enter Slot to use: 1 for coins, 2 for notes 3 for cards");
				
				int slot=1;
				
				
				
				if(slot==1)
					 usedSlot=coinSlot;
				else if(slot==2)
					 usedSlot=notesSlot;
				else if(slot==3)
					 usedSlot=cardSlot;
				else {
					System.err.println("Invalid slot chosen");
					fail("invalid choice for slot");
					
				}
				
				try {
			  isenough=snacksMachineInstance.insertMoney(usedSlot, amt, "USD");
				}catch(InvalidAmountOrCurrencyException e) {
					System.err.println("Amount-slot combination was wrong");
					fail("Amount-slot combination was wrong");
					
				}
			  
			}
			
			
			
			
		
		
	}