package dev.freightos.test24.model;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Class Display
 */
public class Display {
Logger logger;
  //
  // Fields
  //

  
  //
  // Constructors
  //
  public Display () {
	  this.logger=Logger.getLogger(getClass().getName());
	  
  }

public void display(String msg) {
	
	logger.log(Level.INFO, msg);
};
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  //
  // Other methods
  //

}
