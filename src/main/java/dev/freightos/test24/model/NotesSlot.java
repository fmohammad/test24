package dev.freightos.test24.model;

import dev.freightos.test24.controller.MoneySlot;

/**
 * Class NotesSlot
 */
public class NotesSlot implements MoneySlot {

  //
  // Fields
  //

  
  //
  // Constructors
  //
  public NotesSlot () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

@Override
	public float insertMoney(int amount) {
		return amount;
	}
}
