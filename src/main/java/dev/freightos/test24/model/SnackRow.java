package dev.freightos.test24.model;

/**
 * Class SnackRow
 */
public class SnackRow {

  //
  // Fields
  //

  private SnacksColumn snacksColumn;
  
  //
  // Constructors
  //
  public SnackRow () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of snacksColumn
   * @param newVar the new value of snacksColumn
   */
  public void setSnacksColumn (SnacksColumn newVar) {
    snacksColumn = newVar;
  }

  /**
   * Get the value of snacksColumn
   * @return the value of snacksColumn
   */
  public SnacksColumn getSnacksColumn () {
    return snacksColumn;
  }

  //
  // Other methods
  //

}
