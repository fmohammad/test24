package dev.freightos.test24.model;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Class MonySlots
 */
public class MonySlots {

  //
  // Fields
  //

  private CoinSlot coinSlot;
  private CardSlot cardSlot;
  private NotesSlot noteSlot;
  private Logger logger;
  
  //
  // Constructors
  //
  public MonySlots () { 
	  logger=logger.getLogger(getClass().getName());
	  
  };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of coinSlot
   * @param newVar the new value of coinSlot
   */
  public void setCoinSlot (CoinSlot newVar) {
    coinSlot = newVar;
  }

  /**
   * Get the value of coinSlot
   * @return the value of coinSlot
   */
  public CoinSlot getCoinSlot () {
    return coinSlot;
  }

  /**
   * Set the value of cardSlot
   * @param newVar the new value of cardSlot
   */
  public void setCardSlot (CardSlot newVar) {
    cardSlot = newVar;
  }

  /**
   * Get the value of cardSlot
   * @return the value of cardSlot
   */
  public CardSlot getCardSlot () {
    return cardSlot;
  }

  /**
   * Set the value of noteSlot
   * @param newVar the new value of noteSlot
   */
  public void setNoteSlot (NotesSlot newVar) {
    noteSlot = newVar;
  }

  /**
   * Get the value of noteSlot
   * @return the value of noteSlot
   */
  public NotesSlot getNoteSlot () {
    return noteSlot;
  }

public void dispenseChange(float change) {
logger.log(Level.INFO, "change dispensed succesfully");
	
}

  //
  // Other methods
  //

}
