package dev.freightos.test24.model;

import java.util.ArrayList;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import dev.freightos.test24.controller.CustomerInterface;
import dev.freightos.test24.controller.MoneySlot;
import dev.freightos.test24.controller.SnacksMachineInterface;
import dev.freightos.test24.controller.flow.InvalidAmountOrCurrencyException;
import dev.freightos.test24.controller.flow.InvalidVendingMachineState;
import dev.freightos.test24.controller.flow.ItemNotAvialableException;

/**
 * Class SnackMachine
 */
public class SnackMachine implements SnacksMachineInterface {

  //
  // Fields
  //

  private MonySlots monySlots;
  
  private SnacksSlots snacksSlots;
  private Keypad keypad;
  private Display display;
  private Logger logger;
  private float accumulatedAmount;
  private List<Item> items=new ArrayList<>();
  private List<Item> avialableItems=new ArrayList<>();
  private CustomerInterface customer;
  //
  // Constructors
  //
  public SnackMachine (CustomerInterface customer) { 
	  this.customer=customer;
	  this.keypad=new Keypad();
	  this.snacksSlots=new SnacksSlots();
	  this.monySlots=new MonySlots();
	  this.monySlots.setCardSlot(new CardSlot());
	  this.monySlots.setCoinSlot(new CoinSlot());
	  this.monySlots.setNoteSlot(new NotesSlot());
	 this.display=new Display();
	 this.logger=Logger.getLogger(this.getClass().getName());
	 logger.debug("SnackMachine instance just created");
  };
  @Override
	public CardSlot getCardSlot() {
	
		return this.monySlots.getCardSlot();
	}
  @Override
	public CoinSlot getCoinSlot() {

		return this.monySlots.getCoinSlot();
	}
  
  @Override
	public NotesSlot getNotesSlot() {
	
		return this.monySlots.getNoteSlot();
	}
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of monySlots
   * @param newVar the new value of monySlots
   */
  public void setMonySlots (MonySlots newVar) {
    monySlots = newVar;
  }

  /**
   * Get the value of monySlots
   * @return the value of monySlots
   */
  public MonySlots getMonySlots () {
    return monySlots;
  }

  /**
   * Set the value of snacksSlots
   * @param newVar the new value of snacksSlots
   */
  public void setSnacksSlots (SnacksSlots newVar) {
    snacksSlots = newVar;
  }

  /**
   * Get the value of snacksSlots
   * @return the value of snacksSlots
   */
  public SnacksSlots getSnacksSlots () {
    return snacksSlots;
  }

  /**
   * Set the value of keypad
   * @param newVar the new value of keypad
   */
  public void setKeypad (Keypad newVar) {
    keypad = newVar;
  }

  /**
   * Get the value of keypad
   * @return the value of keypad
   */
  @Override
public Keypad getKeypad () {
    return keypad;
  }

  /**
   * Set the value of display
   * @param newVar the new value of display
   */
  public void setDisplay (Display newVar) {
    display = newVar;
  }

  /**
   * Get the value of display
   * @return the value of display
   */
  public Display getDisplay () {
    return display;
  }




  /**
   * 2. The customer selects a number by pressing on the keypad.
   * 3. The VM displays a message that the snack is available for the selected number and
 displays its price.
   */
 	
  @Override
public void  chooseItemForPurchase(Keypad keypad){
logger.debug(" enter ::::: chooseItemForPurchase ");
	 Item item=searchForItemByID(keypad.keyPressed());
      if(item !=null)	 
    	  items.add(item);
      else throw new ItemNotAvialableException("select item is not available in the shopping list");
	  
      display.display("The snack is available for the selected number -- Total price is: "+getTotalPrice());
      
logger.debug(" exit ::::: chooseItemForPurchase ");	 
	 
  }
	 
  private Item searchForItemByID(int itemNo) {
	  logger.debug(" enter ::::: searchForItemByID ");
	  for(Item item:this.avialableItems) {
		  if(item.getItemNo()==itemNo) {
			  logger.debug(" exit ::::: searchForItemByID ");
			  return item;
		  }
	  }
	  logger.debug(" exit ::::: searchForItemByID with null ");
	  return null;
}

/**
   * 4. The customer inserts the money.
	*5. The VM validates the money.
	*6. The VM accepts the money.
	*7. The VM displays the accumulated amount of money each time a new money is entered.
	*8. The VM monitors the amount of the accepted money, If the money is enough, the VM
	*dispenses the selected snack to the customer.
	*9. The VM determines if any change should be sent back to customer.
   * @param slot
   * @param amount
   * @param currency
   * @return
   */
	@Override
	public boolean insertMoney(MoneySlot slot,float amount, String currency){
		  logger.debug(" enter ::::: insertMoney "+ slot+"  "+ amount+" "+currency);
		
		logger.log(Level.INFO, amount+" was inserted for customer: "+customer);
		
		if(!validateMoney(slot,amount,currency)){
			logger.error("Invalid amount or currency was inserted");	
			throw new InvalidAmountOrCurrencyException("Invalid amount or currency was inserted "+ currency);
		}
		
		if(avialableItems==null||avialableItems.size()==0) {
			logger.error("avilable items must be populated first");
			throw new InvalidVendingMachineState("avilable items must be populated first");
		}
		
		accumulatedAmount+=amount;
		
		display.display("Accumulated amount "+accumulatedAmount);


		if(getTotalPrice()<=accumulatedAmount) {
			snacksSlots.dispense();
			
			float change=accumulatedAmount-getTotalPrice();
			if(change>0) {
				display.display("you have "+ change +" lef in change ");
				monySlots.dispenseChange(change);
				
			}
			
			items.clear();
			accumulatedAmount=0;
			
			display.display("selected item(s) has/have been dispensied  ***********bon appetit:)********** ");

			  logger.debug(" exit ::::: insertMoney with true");
			return true;
		}
		
		logger.debug(" exit ::::: insertMoney with false");
		return false;
	}
	
	
	/**
	 * 5. The VM validates the money.
	 * @return
	 */
	@Override
	public boolean validateMoney(MoneySlot slot,float amount, String currency) {
		logger.debug(" enter ::::: validateMoney ");
		if(currency==null||!currency.equalsIgnoreCase("usd")) {
			logger.debug(" --> ::::: validateMoney-> invalid currency ");
			return false;
		}
		if(slot instanceof CoinSlot)
			if(amount!=.10&&amount!=.20&&amount!=.50&&amount!=1) {
				logger.debug(" --> ::::: validateMoney-> invalid amount in coins ");
				return false;
			}
		if(slot instanceof NotesSlot)
			if(amount!=20&&amount!=50) {
				logger.debug(" --> ::::: validateMoney-> invalid amount in notes ");
				return false;
			}
		logger.debug(" exit  ::::: validateMoney with true");
	return true;
	
	}
	
	
	private float getTotalPrice() {
		logger.debug(" enter ::::: getTotalPrice ");

	float tot=0;
	for (Item i:items ) {
		tot+=i.getItemPrice();
	}

	logger.debug(" exit  ::::: getTotalPrice with total accumulated "+tot);
	return tot;
		
	}
	
	
	
	// dumy list
	@Override
	public void populateAvialableItems(List<Item> items) {
	    if(items!=null)
		this.avialableItems=items;	
		
	}

}