package dev.freightos.test24.model;

/**
 * Class SnacksSlots
 */
public class SnacksSlots {

  //
  // Fields
  //

  private SnackRow snacksRow;
  
  //
  // Constructors
  //
  public SnacksSlots () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of snacksRow
   * @param newVar the new value of snacksRow
   */
  public void setSnacksRow (SnackRow newVar) {
    snacksRow = newVar;
  }

  /**
   * Get the value of snacksRow
   * @return the value of snacksRow
   */
  public SnackRow getSnacksRow () {
    return snacksRow;
  }

public void dispense() {
	// TODO Auto-generated method stub
	
}

  //
  // Other methods
  //

}
