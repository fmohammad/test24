package dev.freightos.test24.model;

import dev.freightos.test24.controller.MoneySlot;

/**
 * Class CardSlot
 */
public class CardSlot implements MoneySlot {

  //
  // Fields
  //

  
  //
  // Constructors
  //
  public CardSlot () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

@Override
	public float insertMoney(int amount) {
		return amount;
	}
}
