package dev.freightos.test24.model;

/**
 * Class Keypad
 */
public class Keypad {

	private int keyJustpressed;
	
  //
  // Fields
  //

  
  //
  // Constructors
  //
  public Keypad () { };


  public void setKeyJustpressed(int keyJustpressed) {
	this.keyJustpressed = keyJustpressed;
}



public int keyPressed() {

	return keyJustpressed;
}


}
