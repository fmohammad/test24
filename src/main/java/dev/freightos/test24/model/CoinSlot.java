package dev.freightos.test24.model;

import dev.freightos.test24.controller.MoneySlot;

/**
 * Class CoinSlot
 */
public class CoinSlot implements MoneySlot{

  //
  // Fields
  //

  
  //
  // Constructors
  //
  public CoinSlot () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

@Override
	public float insertMoney(int amount) {

		return amount;
	}
}
