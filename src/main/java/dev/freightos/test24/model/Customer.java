package dev.freightos.test24.model;

import dev.freightos.test24.controller.CustomerInterface;

public class Customer implements CustomerInterface{
	private int customerNumber;
	
	
public Customer(int customerNumber) {
		super();
		this.customerNumber = customerNumber;
	}


@Override
public int getCustomerNumber() {
	return customerNumber;
}

@Override
public String toString() {
	return "Customer id is :"+ customerNumber;
}
}
