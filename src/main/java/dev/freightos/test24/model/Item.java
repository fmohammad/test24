package dev.freightos.test24.model;


/**
 * Class Item
 */
public class Item {

  //
  // Fields
  //

  private int itemNo;
  private String itemName;
  private float itemPrice;
  
  
  public Item(int itemNo, String itemName, float itemPrice) {
	super();
	this.itemNo = itemNo;
	this.itemName = itemName;
	this.itemPrice = itemPrice;
}

public float getItemPrice() {
	return itemPrice;
}

public void setItemPrice(float itemPrice) {
	this.itemPrice = itemPrice;
}

//
  // Constructors
  //
  public Item () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of itemNo
   * @param newVar the new value of itemNo
   */
  public void setItemNo (int newVar) {
    itemNo = newVar;
  }

  /**
   * Get the value of itemNo
   * @return the value of itemNo
   */
  public int getItemNo () {
    return itemNo;
  }

  /**
   * Set the value of itemName
   * @param newVar the new value of itemName
   */
  public void setItemName (String newVar) {
    itemName = newVar;
  }

  /**
   * Get the value of itemName
   * @return the value of itemName
   */
  public String getItemName () {
    return itemName;
  }



}
