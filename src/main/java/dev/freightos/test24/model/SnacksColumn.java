package dev.freightos.test24.model;

/**
 * Class SnacksColumn
 */
public class SnacksColumn {

  //
  // Fields
  //

  private int columnNumber;
  
  //
  // Constructors
  //
  public SnacksColumn () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of columnNumber
   * @param newVar the new value of columnNumber
   */
  public void setColumnNumber (int newVar) {
    columnNumber = newVar;
  }

  /**
   * Get the value of columnNumber
   * @return the value of columnNumber
   */
  public int getColumnNumber () {
    return columnNumber;
  }



}
