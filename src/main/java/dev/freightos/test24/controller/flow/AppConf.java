package dev.freightos.test24.controller.flow;

import java.util.ArrayList;
import java.util.List;

import dev.freightos.test24.controller.SnacksMachineInterface;
import dev.freightos.test24.model.Item;

public class AppConf {
	public static final String flow="many"; // basic many ...
	public static boolean runTestSuite=true;

	public static void populateForTest(SnacksMachineInterface	snacksMachineInstance) {
		 List<Item>	items=new ArrayList<>();
		

		items.add(new Item(1,"item1",12.0f));
		items.add(new Item(2,"item2",2.5f));
		items.add(new Item(3,"item3",42.0f));
		items.add(new Item(4,"item4",7.3f));
		items.add(new Item(5,"item5",.99f));
		items.add(new Item(6,"item6",100.1f));
		items.add(new Item(7,"item7",21.10f));
		items.add(new Item(8,"item8",69.1f));
		snacksMachineInstance.populateAvialableItems(items);
			

		
	}
	
}
