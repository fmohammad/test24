package dev.freightos.test24.controller.flow;

public class InvalidVendingMachineState extends RuntimeException {

	public InvalidVendingMachineState(String message) {
	super(message);
	}
	
}
