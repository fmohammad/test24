package dev.freightos.test24.controller.flow;

public class ItemNotAvialableException extends RuntimeException {

	public ItemNotAvialableException(String message) {
		super(message);
	}

}
