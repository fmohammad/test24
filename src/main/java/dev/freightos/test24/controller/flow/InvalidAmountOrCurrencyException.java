package dev.freightos.test24.controller.flow;

public class InvalidAmountOrCurrencyException extends RuntimeException {

	public InvalidAmountOrCurrencyException(String message) {
		super(message);
	}

}
