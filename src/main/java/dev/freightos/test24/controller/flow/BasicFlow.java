package dev.freightos.test24.controller.flow;

import java.util.ArrayList;
import java.util.List;

import dev.freightos.test24.controller.CustomerInterface;
import dev.freightos.test24.controller.HandleInstances;
import dev.freightos.test24.controller.SnacksMachineInterface;
import dev.freightos.test24.model.Item;
import dev.freightos.test24.model.Keypad;

public class BasicFlow {
	public  void populate(SnacksMachineInterface	snacksMachineInstance) {

	AppConf.populateForTest(snacksMachineInstance);
		
	}

	
	public  void handleBasicFlow() {
		
		CustomerInterface customer=HandleInstances.getCustomerInterfaceInstance();
		
		
		SnacksMachineInterface	snacksMachineInstance =HandleInstances.getSnacksMachineInstance(customer);
		populate(snacksMachineInstance);

		Keypad keypad=snacksMachineInstance.getKeypad();
		keypad.setKeyJustpressed(1);
		
		snacksMachineInstance.chooseItemForPurchase(keypad);
		
		boolean isenough=false;
		while(!isenough) {
		  isenough=snacksMachineInstance.insertMoney(snacksMachineInstance.getCardSlot(), 100, "USD");
		}
		
		
		
		
	
	
}


}