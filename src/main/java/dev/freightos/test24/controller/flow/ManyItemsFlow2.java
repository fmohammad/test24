package dev.freightos.test24.controller.flow;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import dev.freightos.test24.controller.CustomerInterface;
import dev.freightos.test24.controller.HandleInstances;
import dev.freightos.test24.controller.MoneySlot;
import dev.freightos.test24.controller.SnacksMachineInterface;
import dev.freightos.test24.model.Item;
import dev.freightos.test24.model.Keypad;

public class ManyItemsFlow2 {
	public  void populate(SnacksMachineInterface	snacksMachineInstance) {

	AppConf.populateForTest(snacksMachineInstance);
		
	}
	
	
	/**
	 * Many Items selection Flow ~ Valid Scenario
1. This use case begins when the customer wants to purchase snacks.
2. The customer selects a number by pressing on the keypad. 
3. The VM displays a message that the snack is available for the selected number and displays its price.
4. The customer selects another number by pressing on the keypad. 
5. 3,4 repeat many more than once until the customer satisfied with items to purchase
6. The customer inserts the money.
7. The VM validates the money.
8. The VM accepts the money. 
9. The VM displays the accumulated amount of money each time a new money is entered.
10. The VM monitors the amount of the accepted money, If the money is enough, the VM dispenses the selected snack to the customer. 
11. The VM determines if any change should be sent back to customer.
12. The VM displays the change at panel. 
13. Then, the VM dispenses change.	
	 */
	
	public  void handleManyItemsFlow() {
		Scanner scanner=new Scanner(System.in);
		
		CustomerInterface customer=HandleInstances.getCustomerInterfaceInstance();

		SnacksMachineInterface	snacksMachineInstance =HandleInstances.getSnacksMachineInstance(customer);
		populate(snacksMachineInstance);
		Keypad keypad=snacksMachineInstance.getKeypad();

		int select =0;
		boolean atLeastOneIsValid=false;
		/**
		 * keep in the loop until at least one available snack is selected 
		 */
		while(select!=-1 || !atLeastOneIsValid) {    
			System.out.println("Enter Item to purchase");
			select=scanner.nextInt();
			try {
			keypad.setKeyJustpressed(select);
			snacksMachineInstance.chooseItemForPurchase(keypad);
			atLeastOneIsValid=true;
			}catch(ItemNotAvialableException e) {
				System.err.println("item not available");
			}
		}
		
		
		
		boolean isenough=false;
		MoneySlot coinSlot=snacksMachineInstance.getCoinSlot();
		MoneySlot notesSlot=snacksMachineInstance.getNotesSlot();
		MoneySlot cardSlot=snacksMachineInstance.getCardSlot();
		MoneySlot usedSlot=null;
		
		while(!isenough) {
			System.out.println("Enter amount to use");
			float amt=scanner.nextFloat();
			System.out.println("Enter Slot to use: 1 for coins, 2 for notes 3 for cards");
			int slot=scanner.nextInt();
			
			
			if(slot==1)
				 usedSlot=coinSlot;
			else if(slot==2)
				 usedSlot=notesSlot;
			else if(slot==3)
				 usedSlot=cardSlot;
			else {
				System.err.println("Invalid slot chosen");	
				continue;
			}
			try {
		  isenough=snacksMachineInstance.insertMoney(usedSlot, amt, "USD");
			}catch(InvalidAmountOrCurrencyException e) {
				System.err.println("Amount-slot combination was wrong");
				
			}
		  
		}
		
		
		
		
	
	
}


}