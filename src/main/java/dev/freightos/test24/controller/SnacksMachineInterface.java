package dev.freightos.test24.controller;

import java.util.List;

import dev.freightos.test24.model.CardSlot;
import dev.freightos.test24.model.CoinSlot;
import dev.freightos.test24.model.Item;
import dev.freightos.test24.model.Keypad;
import dev.freightos.test24.model.NotesSlot;

public interface SnacksMachineInterface extends VendingMachine{

	/**
	   * Get the value of keypad
	   * @return the value of keypad
	   */
	Keypad getKeypad();

	/**
	   * 2. The customer selects a number by pressing on the keypad.
	   * 3. The VM displays a message that the snack is available for the selected number and
	 displays its price.
	   */

	void chooseItemForPurchase(Keypad keypad);

	/**
	   * 4. The customer inserts the money.
		*5. The VM validates the money.
		*6. The VM accepts the money.
		*7. The VM displays the accumulated amount of money each time a new money is entered.
		*8. The VM monitors the amount of the accepted money, If the money is enough, the VM
		*dispenses the selected snack to the customer.
		*9. The VM determines if any change should be sent back to customer.
	   * @param slot
	   * @param amount
	   * @param currency
	   * @return
	   */
	boolean insertMoney(MoneySlot slot, float amount, String currency);

	/**
	 * 5. The VM validates the money.
	 * @return
	 */
	boolean validateMoney(MoneySlot slot, float amount, String currency);

	// dumy list
	void populateAvialableItems(List<Item> items);

	


	public CardSlot getCardSlot();
	public CoinSlot getCoinSlot();
	public NotesSlot getNotesSlot();
	
}
