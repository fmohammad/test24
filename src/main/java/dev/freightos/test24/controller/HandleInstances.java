package dev.freightos.test24.controller;

import java.util.Random;

import dev.freightos.test24.model.Customer;
import dev.freightos.test24.model.SnackMachine;

public class HandleInstances {
	
	
public static SnacksMachineInterface getSnacksMachineInstance(CustomerInterface customer) {

	return new SnackMachine(customer);
}

public static CustomerInterface getCustomerInterfaceInstance() {

	return new Customer(new Random().nextInt());
}



}
