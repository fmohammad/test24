package dev.freightos.test24.main;

import org.apache.log4j.BasicConfigurator;

import dev.freightos.test24.controller.flow.AppConf;
import dev.freightos.test24.controller.flow.BasicFlow;
import dev.freightos.test24.controller.flow.ManyItemsFlow2;

public class Main {
	public static void main(String[] args) {
		BasicConfigurator.configure();
		

		/**
		 * Run basic flow
		 */
		if(AppConf.flow.equals("basic")) {
		BasicFlow bf=new BasicFlow();
		bf.handleBasicFlow();
		}
		
		if(AppConf.flow.equals("many")) {
			ManyItemsFlow2 mf=new ManyItemsFlow2();
			mf.handleManyItemsFlow();
		}
		

	
	}
}
